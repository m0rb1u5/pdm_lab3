package com.m0rb1u5.primeraaplicacion;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by cc063-JCarbajal on 13/01/2016.
 */
public class SegundaActividad extends AppCompatActivity{

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_segunda_actividad);
   }
}
